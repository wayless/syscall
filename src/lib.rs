/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#![no_std]
#![feature(lang_items)]
extern crate unwind;

#[no_mangle]
pub extern "C" fn _PDCLIB_Exit( status: i32 ) {
    panic!();
}
#[no_mangle]
pub extern "C" fn _PDCLIB_allocpages( pages: i64 ) -> *mut u8{
    0 as *mut u8
}
#[no_mangle]
pub extern "C" fn _PDCLIB_freepages( addr: *mut u8, pages: i64 ) {
}
#[no_mangle]
pub extern "C" fn _PDCLIB_open( 
    fd: *mut u8, //_PDCLIB_fd_t* fd, 
    ops: *mut *mut u8,//const _PDCLIB_fileops_t** ops, 
    filename: *mut u8, 
    mode: u32
    ) -> bool {
    false
}
#[no_mangle]
pub extern "C" fn _PDCLIB_rename( old_file: *mut u8, new_file: *mut u8 ) -> i32{
    1
}
